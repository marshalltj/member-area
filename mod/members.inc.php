<?php
//Helper functions that are used across multiple files for the member area

//search the database to see if $item exists in the given field. Valid fields are userid or email. 
function item_exists($item, $field){
	//blank items are not checked
	if (strlen($item)==0)	
		return false;

	//setup connection to DB
	$con = sql_connect();

	if (strcmp($field, 'email') == 0)
		$result = mysqli_query($con, "SELECT * FROM users WHERE email = '$item';");

	else if (strcmp($field, 'userid') == 0)
		$result = mysqli_query($con, "SELECT * FROM users WHERE userid = '$item';");
	
	if (mysqli_num_rows($result)==0){
		mysqli_close($con);
		return false;
	}
	echo ("<span style='color:red;'><b>'$item' already exists. Please try again.</b></span><br>");
	mysqli_close($con);
	return true;
}

/*
Resetting password functionality. Called when a user registers, or when the forgot 
password tool is used, or when an admin resets a user's password. Sends an email to $email
with a new generated password for the account. If it's a user it will destroy the session (logs them out).
*/
function send_password($email, $usr){
	//setup connection to DB
	$con = sql_connect();
	
	//generate password
	$pw = generatePassword(8);
	$encrypted_pw = md5($pw);

	//set password in DB
	mysqli_query($con, "UPDATE users SET password='$encrypted_pw' WHERE userid = '$usr';");
	mysqli_close($con);

	//email new password
	//headers 
	$headers =  "From: <no-reply@gratonrancheria.com>" . "\r\n";
	$headers .= "To: <" . $email . ">\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

	$subject = 'Your Graton Rancheria account password has been reset';

	//message html
	$message = "
		Your Graton Rancheria password has been reset. This is either because you have registered your account, or you 
		or an administrator has used the password reset tool.<br><br>New password: " .$pw . "<br><br>You can return 
		to our site and log in <a href='http://www.gratonrancheria.com/members'>here</a>.<br>Once logged in, 
		you can change your password using the edit account tool.
	";

	//send the email
	mail($email, $subject, $message, $headers);
	//Logout if not an admin
	if (isset($_SESSION))
		$_SESSION = array();

	echo("<br><b>A new password has been sent to the user's registered email.</b><br>");
}

function sql_connect(){
	return mysqli_connect('****', '****', '****', '****');
}

//Generates a random password of letters/numbers. Vowels taken out to avoid awkward passwords.
function generatePassword($length) {
  $chars = 'bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ0123456789';
  $count = mb_strlen($chars);

  for ($i = 0, $result = ''; $i < $length; $i++) {
    $index = rand(0, $count - 1);
    $result .= mb_substr($chars, $index, 1);
  }
  return $result;
}
?>