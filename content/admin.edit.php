<?php
	//if they got here not through the admin area, redirect
	if (empty($_POST))
		echo ('<script type="text/javascript">window.location = "/admin/";</script>');
		
	include "mod/members.inc.php";
	$usr = $_POST['adminEdit'];

	// a submit button has been pressed, update user info
	if (isset($_POST['oldId'])){
		//if it was the reset pass button
		if ($_POST['submit'] == 'Reset User Password'){
			send_password($_POST['oldEmail'], $_POST['oldId']);
			$usr = $_POST['oldId'];
		}

		//if changes have been submitted, change them
		else if ($_POST['submit'] == 'Save User Info'){
			$usr = update_account();
			//$usr = $_POST['oldId'];
		}
		//else, delete button was pressed
		else{
			delete_user($_POST['oldId']);
			$deleted = True;
		}
	}

	// only show this info if we haven't delted the user
	if (!$deleted){

		//find info using ID.
		$con = sql_connect();
		$query = mysqli_query($con, "SELECT * FROM users WHERE userid = '$usr';");
		$result = mysqli_fetch_array($query);
		$email = $result['email']; $phone = explode(',',$result['numbers']);

		//disable reset password button if user is not registered
		$disable = ($result['verified'] == 0 ? 'disabled' : '');
?>

<form class='adminEditForm' action='/admin/edit/' method='post'>
	Current User ID: <b><?php echo($usr); ?></b><br>
	<label>New User ID</label>
	<input type='hidden' name='oldId' value="<?php echo($usr) ?>">
	<input type='text' name='usrId'><br><br>
	Current Email: <b><?php echo($email); ?></b><br>
	<label>New Email</label>
	<input type='hidden' name='oldEmail' value="<?php echo($email); ?>">
	<input type='email' name='new_email' <?php echo($disable); ?>>
	
	<?php
	if ($result['verified'] == 0){
	?>	
		<br><br>
		<label>Set Birthdate</label>
		<input type='text' name='newBirthday'>
	<?php
	}
	?>

	<br><br>
	<input type='submit' value='Save User Info' name='submit'>&nbsp&nbsp&nbsp
	<input type='submit' value='Reset User Password' name='submit' <?php echo($disable); ?>>&nbsp&nbsp&nbsp
	<input type='submit' value='Delete User' name='submit' onClick="return confirm('Are you sure?')"><br>
	<small>
		To change a user's email AND reset their password, change the email and click 'Save User Info', 
		then use the password reset button
	</small><br>
</form>
<br>

<?php 
	mysqli_close($con); 
}
?>
<br><a href='/admin/?key=****'>Go back</a>

<?php
	/*** FUNCTIONS ***/
	function update_account(){
		$usr = $_POST['oldId'];
		$return_usr = $usr;
		$newId = $_POST['usrId'];

		//setup connection to DB
		$con = sql_connect();
		$query = mysqli_query($con, "SELECT * FROM users WHERE userid = '$usr';");
		$result = mysqli_fetch_array($query);

		//if they submitted a new email
		if (strlen($_POST['new_email']) != 0 && !item_exists($_POST['new_email'], 'email')){
			$email = $_POST['new_email'];

			//update the email
			mysqli_query($con, "UPDATE users SET email='$email' WHERE userid = '$usr';");
			echo ("<br><b>Email Updated</b><br><br>");
		}

		//if they submitted a birthday
		if (strlen($_POST['newBirthday']) != 0){
			$birthday = md5($_POST['newBirthday']);
			//update the password
			mysqli_query($con, "UPDATE users SET password='$birthday' WHERE userid = '$usr';");
			//dont post the 'updated' if it's an admin editing
			echo ("<br><b>Password Updated</b><br><br>");
		}

		//if they submitted a new userid
		if (strlen($newId) != 0 && !item_exists($newId, 'userid')){
			//update the id
			mysqli_query($con, "UPDATE users SET userid='$newId' WHERE userid = '$usr';");
			$return_usr = $newId;
			echo ("<br><b>User ID Updated<br></b><br><br>");
		}
		mysqli_close($con);
		return $return_usr;
	}

	//deletes a user account given an id
	function delete_user($id){
		echo("<br><b>User '$id' Deleted</b><br><br>");
		//setup connection to DB
		$con = sql_connect();
		mysqli_query($con, "DELETE FROM users WHERE userid='$id'");
		mysqli_close($con);
	}
?>
