<script type="text/javascript">
	/*This function needs to be attached to BOTH input fields you are comparing, with the same parameters in the 
	same order in BOTH*/
	function check(input, id) {
		if (document.getElementById(input).value != document.getElementById(id).value) {
			document.getElementById(input).setCustomValidity('Fields must be matching.');
		} 
		else {
			// input is valid -- reset the error message
			document.getElementById(input).setCustomValidity('');
		}
	};
</script>

<?
	include 'mod/members.inc.php';
 	//attempting to login.
	if (!empty($_POST) && empty($_SESSION))
		login($_POST['user'],$_POST['pw']);

	//logged in
	if (!empty($_SESSION) && !isset($_SESSION['register'])){
		echo("
			<a href='/members/view?category=calendar'>Calendars</a><br>
			<a href='/members/view?category=newsletter'>Newsletters</a><br>
			<a href='/members/view?category=form'>Forms</a><br>
			<br>
			<a href='/members/edit'>Edit Account</a> | <a href='/members/logout'>Log Out</a>
		");
	}

	//first time logging in, need to register user
	else if (isset($_SESSION['register'])){
		//Process registration form
		if (isset($_POST['email']) && !item_exists($_POST['email'], 'email')){
			register_user($_POST['email']);
			$temp = $_SESSION['userid'];
			send_password($_POST['email'], $_SESSION['userid']);
			echo("
				<h3>Login to continue</h3>
				<form id='loginForm' action='/members/' method='post'>
					<label>Username</label>
					<input type='text' name='user' value='$temp' required><br>
					<label>Password</label>
					<input type='password' name='pw' required>
					<input type='submit' value='Submit'>
				</form>
				<a href='/members/forgot'>Forgot your password?</a><br><br>
				<small>If this is your first time logging in, use your birthday formatted <i>MMDDYYYY</i> (For example, 
				March 14th, 1990 would be entered as 03141990) as your password. If you can't log in, 
				please <a href='/contact'>contact us</a>.</small>
			");
		}
		//show registration form
		else
			echo("
				We see this is your first time logging in. Please register your account by providing a valid email address.
				<br><br>
				<form id='registerForm' action='/members/' method='post'>
					<label>Email</label>
					<input type='email' name='email' oninput='check(\"confirm_email\", \"email\")' id='email' required>
					<br>
					<label>Confirm Email</label>
					<input type='email' oninput='check(\"confirm_email\", \"email\")' id='confirm_email' required>
					<input type='submit' value='Submit'>
				</form><br>
				<a href='/members/logout'>Cancel</a>
			");
	}

	else{
		echo("
			<h3>Login to continue</h3>
			<form id='loginForm' action='/members/' method='post'>
				<label>Username</label>
				<input type='text' name='user' required><br>
				<label>Password</label>
				<input type='password' name='pw' required>
				<input type='submit' value='Submit'>
			</form>
			<a href='/members/forgot'>Forgot your password?</a><br><br>
			<small>If this is your first time logging in, use your birthday formatted <i>MMDDYYYY</i> (For example, 
				March 14th, 1990 would be entered as 03141990) as your password. If you can't log in, 
				please <a href='/contact'>contact us</a>.</small>
		");
	}

/*** Functions ***/
	//registration 
	function register_user($email){
		//setup connection to DB
		$con = sql_connect();
		//get username
		$usr = $_SESSION['userid'];

		//set them up as a verified user
		mysqli_query($con, "UPDATE users SET email='$email', verified=1 WHERE userid = '$usr';");
		mysqli_close($con);
	}

	function login($usr, $pw){
		//getting user from database
		$con = sql_connect();
		$query = mysqli_query($con, "SELECT * FROM users WHERE userid = '$usr';");
		$result = mysqli_fetch_array($query);

		//invalid user
		if (!$result){
			echo('<span style="color:red;"><b>Invalid User ID</b></span>');
			mysqli_close($con);
			return false;
		}	

		//first time login
		if ($result['verified']==0){
			//check phone numbers instead of password field
			if (strcmp(md5($pw),trim($result['password']))==0) {
				session_regenerate_id();
					$_SESSION['register']=true;
					$_SESSION['userid']=$usr;
				session_write_close();
				mysqli_close($con);
				return true;
			}
		}

		//successful login
		else if (strcmp(md5($pw),trim($result['password']))==0) {
			session_regenerate_id();
				$_SESSION['userid']=$usr;
			session_write_close();
			mysqli_close($con);
			return true;
		}

		//invalid password
		echo('<span style="color:red;"><b>Invalid Password</b></span>');
		mysqli_close($con);
		return false;
	}
?>


