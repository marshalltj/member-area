<?php
	include 'mod/members.inc.php';
	if (isset($_POST['reset_id'])){
		//grabbing userid
		$usr = $_POST['reset_id'];

		//setup connection to DB
		$con = sql_connect();

		//find email using ID.
		$query = mysqli_query($con, "SELECT * FROM users WHERE userid = '$usr';");
		$result = mysqli_fetch_array($query);

		//invalid ID
		if($result == NULL)
			echo("
				<span style='color:red;'><b>Invalid User ID</b></span><br>
				Forgot your password? Enter your username and we'll send you a new one to your registered email address.<br>
				<br>
				<form action='http://www.gratonrancheria.com/members/forgot/' method='post'?
					<label>User ID</label>
					<input type='text' name='reset_id' required>
					<input type='submit' value='Submit'>
				</form><br>
				Forgot your User ID? <a href='/contact'>Contact Us</a> for assitance.
			");

		//valid ID, but account has not been registered yet
		else if($result['verified'] == 0)
			echo("
						<span style='color:red;'><b>Your account has not been registered yet</b></span><br><br>
						Your User ID is valid, but you have not registered your account yet. Please return to
						the <a href='epedigree.php'>login page</a> and login using your ID and birthdate to register your 
						account.<br>
						<br>
						If you require assitance with this please <a href='/contact'>contact us</a>.
					");

		//valid, reset PW
		else{
			send_password($result['email'], $usr);
			echo("
				<h3>Login to continue</h3>
				<form id='loginForm' action='/members/' method='post'>
					<label>Username</label>
					<input type='text' name='user' value='$usr' required><br>
					<label>Password</label>
					<input type='password' name='pw' required>
					<input type='submit' value='Submit'>
				</form>
				<a href='/members/forgot'>Forgot your password?</a><br><br>
				<small>If this is your first time logging in, use your birthday formatted <i>MMDDYYYY</i> (For example, 
				March 14th, 1990 would be entered as 03141990) as your password. If you can't log in, 
				please <a href='/contact'>contact us</a>.</small>
			");
		}
		mysqli_close($con);
	}

	else{
		echo("
			Forgot your password? Enter your username and we'll send you a new one to your registered email address.<br>
			<br>
			<form action='http://www.gratonrancheria.com/members/forgot/' method='post'?
				<label>User ID</label>
				<input type='text' name='reset_id' required>
				<input type='submit' value='Submit'>
			</form><br>
			Forgot your User ID? <a href='/contact'>Contact Us</a> for assitance.
		");
	}
?>