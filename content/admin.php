<?php
	include "mod/members.inc.php";
	//Admin authentication
	$loggedIn = False;
	if (strcmp($_POST['password'], "****") == 0)
		$loggedIn = True;
	
	//special key that's added to 'back' links for admins
	if (strcmp($_GET['key'], "****") == 0)
		$loggedIn = True;

	//adding a new user
	if (isset($_POST['new_user_id'])){ 
		$loggedIn = True;
		if (!item_exists($_POST['new_user_id'], 'userid'))
			add_user();
	}

	//uploading a file
	if (isset($_POST['category'])){
		$loggedIn = True;
		uploadFile();
	}

	if ($_POST['submit'] == 'Delete Files'){
		$loggedIn = True;
		deleteFiles();
	}
?>
<script src="/js/admin.jobs.js"></script>
<link rel="stylesheet" type="text/css" href="/style/admin.jobs.css">


<?php 
	if ($loggedIn){ 
?>

<!-- Job management area -->
<h3>Job Tool</h3>
<div id="jobs_index" class="sub_page">
	<div id="application">
		<div id="job_list"></div>
	</div>   
</div>
<br><br>

<button onClick="document.getElementById('help').style.display='block';">Documentation/Help</button>
<!-- Documentation -->
<div id="help" style="display:none;">
	<br>
	<h3>Member Documentation</h3>
	<a href="/members/">/members/</a><br>
	<br>
	<b>First time login for Members:</b><br>
	For first time login, a member will use his/her unique member ID as their username and their birthdate 
	formatted MMDDYYYY as their password. The system will detect if this is their first time logging in, and will 
	prompt them to enter an email address. The email address MUST BE UNIQUE. Once a valid email has been provided, 
	a password will be generated and sent to the submitted email address. From there, they can now log in normally. 
	The user is now considered "registered".<br>
	<br>
	<b>Normal Login for Members:</b><br>
	Once a user is registered, the login process is pretty simple. At the member's area, they will enter their 
	unique member ID as their username, and whatever their password is currently set to for their password. They 
	will then be redirected to member's area where they can view the various forms.
	<br>
	<b>Changing a Password as a Member:</b><br>
	Once a member is logged in, they have the option to change their password. At the bottom of the member's area, 
	there is a link to "Edit Account". Once there, they can update their password by putting their current password 
	in the "Current Password" field, and typing a new password in the "New Password" field and confirming the new 
	password in the "Confirm Password" field by typing the same password they put in the "New Password" field.<br>
	<br>
	<b>Retrieving a new Password (Forgotten Password) as a Member:</b><br>
	If a registered user has forgotten their password, there is a password reset tool that can be used. From the 
	login page, there is a link titled "Forget your password?". Following that link, they will be taken to a form 
	where they can enter their unique member ID. Once they have entereted their ID, an email will be sent with a 
	new generated password that they can now use to log in.<br>
	<br>
	<b>Changing an Email Address as a Member:</b><br>
	Once a member is logged in, they have the option to change their password. At the bottom of the member's area, 
	there is a link to "Edit Account". Once there, they can update their email by putting their new email address 
	in the "New Email" field, and confirming the new email address in the the "Confirm Email" field by typing the 
	same email they put in the "New Email" field.<br>
	<br>
	<h3>Admin Documentation</h3>
	<a href="/admin/">/admin/</a><br>
	admin password: ****<br>
	<br>
	<b>Adding a New User as an Admin:</b><br>
	After logging in to the admin area, there is a form labeled "Add User". Simply assign the user a login ID, and 
	enter their birthday formatted MMDDYYYY. This will set the username and initial password for that user. Press 
	the "Add User" button after entering the data, and the user will be added to the system.<br>
	<br>
	<b>Editing Member information as an Admin:</b><br>
	In the admin area, at the bottom of the page there is a list of members who are in the system. In each row for 
	the members, there is a button titled "Edit/Delete". If you click on this button, it will take you to the edit 
	page for the account. From here, you can <br>
	-Change the user's ID<br>
	-Change the user's email<br>
	-Reset the user's password<br>
	-Delete the user<br>
	User IDs and emails ARE UNIQUE, so you can't have two users with the same email or ID. You can only change the 
	emails/reset password of registered users. When changing user information, simply fill in the fields you want to 
	change, then press the "Save User Info" button. Pressing the "Reset User Password" button will send a new 
	generated password to the user's email address. To delete a user, simply press the "Delete User" button. It will 
	ask you to confirm the deletion.<br>
	<br>
	<b>Uploading Files for Members as an Admin:</b><br>
	In the admin area, there is a section titled "Upload File". This is where you will be uploading files for the 
	members to view. Press the "Browse..." button to select a PDF file to upload. Then choose the category from the 
	drop down menu. Once both are selected, click the upload file button and it will be added to the system.<br>
	<br>
	<b>Deleting Files as an Admin:</b><br>
	In the admin area, under the upload form, you will see a section titled "Delete Files". This will list all the 
	files you have uploaded as well as the category you've uploaded them to. To delete files, just check the 
	checkbox of the file you want to delete, then press the "Delete Files" button after you've checked all the files 
	you wish to delete.<br>
	<br>
	<button onClick="document.getElementById('help').style.display='none';">Close</button>
</div>
<br><br>
<!-- Form to add PDFs -->
<h3>Upload File</h3>
<form action="http://www.gratonrancheria.com/admin/" method="post" enctype="multipart/form-data">
	File: <input type="file" name="fileToUpload" id="fileToUpload" required><br>
  Category: <select name="category">
  	<option value="calendar">Calendars</option>
  	<option value="newsletter">Newsletters</option>
  	<option value="form">Forms</option>
  </select><br>
  <input type="submit" value="Upload File" name="submit">
</form>

<!-- Delete Files -->
<br><br>
<h3>Delete Files</h3>
<form action="http://www.gratonrancheria.com/admin/" method="post">
	<?php
		$con = sql_connect();
		$query = mysqli_query($con, "SELECT * FROM files ORDER BY category");
		echo("
			<table>
				<tr>
					<th>Filename</th>
					<th style='padding-left:20px'>Category</th>
					<th style='padding-left:20px'>Delete?</th>
				</tr>
		");
		$i = 0;
		while ($row = mysqli_fetch_array($query)){
			echo("
				<tr>
					<td>$row[name]</td>
					<td align='center' style='padding-left:20px'>$row[category]</td>
					<td align='center' style='padding-left:20px'><input type='checkbox' name='file" . $i . 
					"' value='$row[id]'></td>
				</tr>
			");
			$i++;
		}
		echo("</table>");
	?>
	<input type="submit" value="Delete Files" name='submit'>
</form>

<!-- Form to add users -->
<br><br>
<h3>Add User</h3>
<form id="loginForm" action='http://www.gratonrancheria.com/admin/' method='post'>
	<label>User ID</label>
	<input type='text' name='new_user_id' required><br>
	<label>Birthdate*</label>
	<input type='text' name='birthday' required><br>
	<input type='submit' value='Add User'>
</form>
<small>*Format birthdays as MMDDYYYY. For example, March 14th, 1990 would be entered as 03141990</small><br><br>

<?php
		echo(displayUsers());
	}

	else{
?>

<b>Restricted Area - Please enter password:</b>
<form id="loginForm" action="http://www.gratonrancheria.com/admin/" method="post">
	<label>Password:</label>
	<input type="password" name="password" required>
	<input type="submit" value="Submit">
</form>

<?php
	}

	/*** FUNCTIONS ***/
	function displayUsers(){
		//variable the controls the number of results per page
		$PAGE_AMOUNT = 20;

		//figure out which page we're on
		if (!isset($_GET['start']))
			$start = 0;
		else
			$start = intval($_GET['start']);
		$queryStart = $start * $PAGE_AMOUNT;

		$html = "
			<h3>Members</h3>
			<form action='http://www.gratonrancheria.com/admin/' method='get'>
				<input type='text' name='query' value='$_GET[query]'>
				<input type='hidden' name='key' value='****'>
				<input type='submit' value='Search'>
			</form>
		";

		//if this is a search
		if (isset($_GET['query'])){
			$html .= "<br><a href='/admin/?key=****'><button>Clear</button></a>";
			$htmlQuery = "&query=$_GET[query]";
			$query =  "SELECT * FROM users WHERE userid LIKE '%$_GET[query]%' OR email LIKE '%$_GET[query]%' 
				ORDER BY userid LIMIT $queryStart, $PAGE_AMOUNT";
		}

		else
			$query = "SELECT * FROM users ORDER BY userid LIMIT $queryStart, $PAGE_AMOUNT";

		$html .= "
			<br>
			<table>
				<thead>
					<tr>
						<th>User ID</th>
						<th>Email</th>
						<th>Registered</th>
						<th width='170'>Edit/Delete User</th>
					</tr>
				</thead>
				<tbody>
		";

		//Grab ALL users from DB
		$con = sql_connect();

		$result = mysqli_query($con, $query);
		//loop over all users and display in a table
		while($row = mysqli_fetch_array($result)){
			//dont display admin account in list of users
			//cleaning up raw data for nicer table view
			$verified = ($row['verified'] == 1 ? 'Yes' : 'No');
			$email = (strlen($row['email']) == 0 ? '---' : $row['email']);

			$html .= "
				<tr>
					<td>$row[userid]</td>
					<td align='center'>$email</td>
					<td align='center'>$verified</td>
					<td align='center'>
						<form action='http://www.gratonrancheria.com/admin/edit/' method='post'>
							<input type='hidden' value='$row[userid]' name='adminEdit'>
							<input type='submit' value='Edit/Delete'>
						</form>
					</td>
				</tr>
			";
		}

		$html .= "
			</tbody></table>
			Page: 
		";

		//pagify
		$total = mysqli_num_rows(mysqli_query($con, str_replace(" LIMIT $queryStart, $PAGE_AMOUNT", "", $query)));
		$pages =  ceil($total/$PAGE_AMOUNT);
		for ($i=0;$i<$pages;$i++){
			$pageView = $i + 1;
			if ($i == $start)
				$html .= "$pageView ";
			else
				$html .= "<a href='admin/?key=****&start=$i" . $htmlQuery . " '>$pageView</a> ";
		}

		mysqli_close($con);
		return $html;
	}

	//adds a user to the database
	function add_user(){
		//setup connection to DB
		$con = sql_connect();

		$id = $_POST['new_user_id']; $pw = md5($_POST['birthday']);
		mysqli_query($con, "INSERT INTO users(userid, password, verified) VALUES ('$id', '$pw', 0)");
		mysqli_close($con);
	}

	//upload a file
	function uploadFile(){
		$target_dir = "uploads/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$fileType = pathinfo($target_file,PATHINFO_EXTENSION);

		// Check if file already exists
		if (file_exists($target_file)) {
		    echo "Sorry, file already exists.<br>";
		    $uploadOk = 0;
		}

		// Allow certain file formats
		if($fileType != "pdf") {
		    echo "Sorry, only PDF files are allowed.<br>";
		    $uploadOk = 0;
		}

		//Check if file contains spaces
		if(strpos($target_file, " ") !== False){
			echo("Sorry, files cannot have spaces in them.<br>");
			$uploadOk = 0;
		}

		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0)
		    echo "Sorry, your file was not uploaded.<br>";

		// if everything is ok, try to upload file
		else {
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        //add file to DB
        $con = sql_connect();
        $name = basename($_FILES["fileToUpload"]["name"]);
       	$category = $_POST['category'];
       	mysqli_query($con, "INSERT INTO files(name, category) VALUES ('$name', '$category')");
				mysqli_close($con);
	    } 
	    else 
	    	echo "Sorry, there was an error uploading your file.";
		}
	}

	//delete selected files from DB and FTP
	function deleteFiles(){
		$con = sql_connect();	
		foreach($_POST as $value){
			if ($value != "Delete Files"){
				$query = mysqli_query($con, "SELECT name FROM files WHERE id=$value");
				$result = mysqli_fetch_array($query);
				$path = __DIR__.'/../uploads/' . $result['name'];
				if (!unlink($path))
					echo("Error deleting " . $result['name']);
				else{
					echo($result['name'] . " was sucessfully deleted.<br>");
					mysqli_query($con, "DELETE FROM files WHERE id=$value");
				}
			}
		}
	}
?>