<?php
	//if they aren't logged in redirect to login page.
	if (empty($_SESSION))
		echo ('<script type="text/javascript">window.location = "epedigree.php";</script>');
	include 'mod/members.inc.php';
?>

<script type="text/javascript">
	/*This function needs to be attached to BOTH input fields you are comparing, with the same parameters in the 
	same order in BOTH*/
	function check(input, id) {
		if (document.getElementById(input).value != document.getElementById(id).value) {
			document.getElementById(input).setCustomValidity('Fields must be matching.');
		} 
		else {
			// input is valid -- reset the error message
			document.getElementById(input).setCustomValidity('');
		}
	};
</script>

<?php
	$usr = $_SESSION['userid'];
	//if a form has been submitted for editing an account you OWN, update account details
	if (!empty($_POST))
		update_account($usr);
?>

User ID: <b><?php echo($usr); ?></b><br>
Email: <b>

<?php
	//find email using ID.
	$con = sql_connect();
	$query = mysqli_query($con, "SELECT * FROM users WHERE userid = '$usr';");
	$result = mysqli_fetch_array($query);
	echo($result['email']);
?>

</b><br><br>
<b>Change Email</b>
<form id='editForm' action='/members/edit/' method='post'>
	<label>New Email</label>
	<input type='email' oninput='check("confirm_email", "email")' name='new_email' id='email'><br>
	<label>Confirm Email</label>
	<input type='email' oninput='check("confirm_email", "email")' id='confirm_email' value=''><br><br>
	<b>Change Password</b><br>
	<label>Current Password</label>
	<input type='password' name='current_password' value=''><br>	
	<label>New Password</label>
	<input type='password' oninput='check("confirm_password", "password")' name='new_password' id='password'><br>
	<label>Confirm Password</label>
	<input type='password' oninput='check("confirm_password", "password")' id='confirm_password'><br>
	<input type='hidden' name='edit_owned_account' value='true'>
	<input type='submit' value='Submit'>
</form><br>
<a href='/members'>Go back</a>

<?php
	mysqli_close($con);

	/*** Functions ***/
	function update_account($usr){
		//setup connection to DB
		$con = sql_connect();
		$query = mysqli_query($con, "SELECT * FROM users WHERE userid = '$usr';");
		$result = mysqli_fetch_array($query);

		//if they submitted a new email
		if (strlen($_POST['new_email']) != 0 && !item_exists($_POST['new_email'], 'email')){
			$email = $_POST['new_email'];

			//update the email
			mysqli_query($con, "UPDATE users SET email='$email' WHERE userid = '$usr';");
			//dont post the 'updated' if it's an admin editing
			echo ("<br><b>Email Updated</b><br><br>");
		}

		//if they submitted a new password
		if (strlen($_POST['new_password']) != 0){
			//valid password
			if (strcmp(md5($_POST['current_password']),trim($result['password']))==0){
				$encrypted_pw = md5($_POST['new_password']);

				//if password is too short
				if (strlen($_POST['new_password']) < 6){
					echo ("
						<br><span style='color:red;'><b>New password too short, minimum length is 6 characters.
						</b></span><br><br>
					");
				}

				else{
					//set password in DB
					mysqli_query($con, "UPDATE users SET password='$encrypted_pw' WHERE userid = '$usr';");
					echo ("<br><b>Password Updated</b><br><br>");
				}
			}

			//invalid password
			else
				echo ("<br><span style='color:red;'><b>Incorrect old password. Please try again.</b></span><br><br>");
		}

		mysqli_close($con);

	}
?>