<?php

//	Jobs index page content
//	1/15/2014
//  Arthur Wuterich
?>
<script src="/js/admin.jobs.js"></script>
<link rel="stylesheet" type="text/css" href="/style/admin.jobs.css">

<div id="jobs_index" class="sub_page">
	<div id="application">
		<div id="job_list"></div>
	</div>   
</div>
