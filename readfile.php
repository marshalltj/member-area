<?php
	ob_start();
	session_start();
	//if they aren't logged in redirect to login page.
	if (empty($_SESSION))
		echo ('<script type="text/javascript">window.location = "/members/";</script>');
	else{
		header("Content-Type: application/pdf");
		echo file_get_contents("uploads/" . $_GET['file']);
	}
?>