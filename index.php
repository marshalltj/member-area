<?php
//	Main content wrapper
//	10/25/2013
//	Arthur Wuterich

// Title of webpage
$title = 'Graton Rancheria';

// Rel path to content folder
$ROOT_PATH = '/nfs/www/WWW_pages/figr/gratonrancheria.com';
$CONTENT_PATH = "{$ROOT_PATH}/content";
$AJAX_PATH = "{$ROOT_PATH}/ajax.php";

// If this is an ajax request then process the request, output the response, and exit
if( ($response = include $AJAX_PATH ) != null )
{
    echo $response;
    return;
}

// Include mod file(s)
include "{$ROOT_PATH}/mod/job.editor.php";

// Break apart the URI
//echo $_SERVER['REQUEST_URI'];
$requestUrl = explode( '/', $_SERVER['REQUEST_URI'] );
$page = '';

// Process the URI request
while( count( $requestUrl ) > 0 )
{
	// If any element is empty skip processing
	if( strlen( reset( $requestUrl ) ) <= 0 )
	{
		array_shift($requestUrl);
		continue;
	}

	// If the current page variable is not empty append a delimiter
	if( strlen($page) > 0 )
	{
		$page .= '.';
	}

	// Add the page level
	$page .= array_shift( $requestUrl );

    // Format out get variables if they are present
    if( ( $pos = strrpos( $page, '?' ) ) !== false )
    {
        $page = substr( $page, 0, $pos-1 );
    };
}

// If the page is not readable then redirect to home
if( strlen( $page ) <= 0 || !is_readable( "{$CONTENT_PATH}/{$page}.php" ) )
{
	$page = 'home';
}
else
{
	// Process the last root node's name as the title
    $page = explode( '?', $page );
    $page = $page[0];
	$pageLevels = explode( '.', $page );
	$titlePage = ucwords( str_replace( array( '-', '_' ), ' ', end( $pageLevels ) ) );
	$title .= " {$titlePage}";
}

//sessions for members areas
ob_start();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<link rel="stylesheet" type="text/css" href="/style/global.css">
<script src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/js/slide_show.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58281821-1', 'auto');
  ga('send', 'pageview');

</script>
<title><?php echo $title; ?></title>
<!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="style/lt.ie8.global.css"><![endif]-->
</head>
<body>
<div id="header_bar"></div>
<div id="content_wrapper" class="site_content_width" >
	<div id="content">
		<?php include "{$CONTENT_PATH}/header.php"; ?>
		<div id="main_content"><?php include "{$CONTENT_PATH}/{$page}.php"; ?></div>
	</div>
</div>
<div id="footer_bar">
	<?php include "{$CONTENT_PATH}/footer.php"; ?>
</div>
</body>
</html>
