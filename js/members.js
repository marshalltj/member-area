/*This function needs to be attached to BOTH input fields you are comparing, with the same parameters in the same order in BOTH*/
function check(input, id) {
	if (document.getElementById(input).value != document.getElementById(id).value) {
		document.getElementById(input).setCustomValidity('Fields must be matching.');
	} 
	else {
		// input is valid -- reset the error message
		document.getElementById(input).setCustomValidity('');
	}
};